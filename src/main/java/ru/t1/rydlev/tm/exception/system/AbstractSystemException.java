package ru.t1.rydlev.tm.exception.system;

import ru.t1.rydlev.tm.exception.AbstractException;

public class AbstractSystemException extends AbstractException {

    public AbstractSystemException() {
    }

    public AbstractSystemException(String s) {
        super(s);
    }

    public AbstractSystemException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public AbstractSystemException(Throwable throwable) {
        super(throwable);
    }

    public AbstractSystemException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }

}
