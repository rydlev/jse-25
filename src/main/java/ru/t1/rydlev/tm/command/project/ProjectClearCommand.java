package ru.t1.rydlev.tm.command.project;

import org.jetbrains.annotations.NotNull;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        getProjectTaskService().removeProjects();
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Clear projects.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

}
