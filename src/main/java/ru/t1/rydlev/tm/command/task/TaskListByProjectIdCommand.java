package ru.t1.rydlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.model.Task;
import ru.t1.rydlev.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("[ENTER PROJECT ID:]");
        @NotNull final String projectID = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @NotNull final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectID);
        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show task list by project id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-project-id";
    }

}
